(* Welcome to the OCaml Starter for Block Battle on theaigames.com
 * Look at ulttt.ml for more information.
 *)
open Td;;

let main gstate =
(*
  let moves = Blockbattle.legal_moves gstate in
  let move = Blockbattle.random_from_list moves in
    Blockbattle.issue_order move
 *)
    Blockbattle.issue_order "left,drop"
;;

Blockbattle.run_bot main

