OCaml starter package for Block Battle
======================================

see theaigames.com for details of the game
------------------------------------------

The provided "main.ml" file is a minimal bot which parses the game state and outputs a fixed set of moves in each round.

As seen in that example, your bot should provide a function which takes a game_state as its argument, and calls issue_order once.

You then call Blockbattle.run_bot, providing this function as the argument.

Have a look at td.ml to see the definition of the game_state data type.

