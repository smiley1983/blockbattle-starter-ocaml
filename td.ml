type piece = [ `I | `J | `L | `O | `S | `T | `Z ];;
type cell = [`Empty | `Shape | `Block | `Solid]

type game_info =
 {
   mutable timebank : int;
   mutable time_per_move : int;
   mutable player_names : string list;
   mutable your_bot : string;
   mutable field_width : int;
   mutable field_height : int;
 }
;;

type player =
{
  mutable row_points : int;
  mutable combo : int;
  mutable skips : int;
  mutable field : cell array array;
}
;;

type game_state =
 {
   mutable round : int;
   mutable this_piece_type : piece;
   mutable next_piece_type : piece;
   mutable this_piece_position : (int * int);
   mutable me : player;
   mutable opponent : player;
   mutable last_update : float;
   mutable last_timebank : int;
   setup : game_info;
 }
;;


