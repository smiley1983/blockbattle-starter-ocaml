(* Welcome to the OCaml Starter for Ultimate TicTacToe on theaigames.com
 * Have a look at the utility functions at the end of this file.
 *
 * Also note that you may want to extend the data structures in td.ml
 * or use these as input-only and create your own data structures to
 * represent the game state. 
 *
 * It may be worth considering collapsing the cell and macro_cell data
 * types into a single type, as this can save you some messy type coercions
 * later. It may have other short-term consequences, I haven't tested this
 * approach in a bot yet.
 *
 * The debug function below is helpful for writing to stderr or a file.
 * Just remove the comment marks and () from the function below if you want 
 * to use it.
 *
 * If you find any bugs, please notify the
 * developer here:
     https://github.com/smiley1983/ultimate_tictactoe_ocaml
 *)

open Td;;

let get_time () = Unix.gettimeofday ();;

let square_size = 3;;

let out_chan = stderr (* open_out "mybot_err.log" *);;

let debug s = ()
(*
   output_string out_chan s; 
   flush out_chan
*)
;;

(* input processing *)

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s
;;


(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []

let action_move bot gstate t2 =
  gstate.last_timebank <- (int_of_string t2);
  bot gstate
;;

let get_bot gstate bot =
  if bot = gstate.setup.your_bot then 
    gstate.me 
  else 
    gstate.opponent
;;

let update_row_points bot v =
  bot.row_points <- int_of_string v
;;

let update_combo bot v =
  bot.combo <- int_of_string v
;;

let update_skips bot v =
  bot.skips <- int_of_string v
;;

let cell_of_string s = 
  match int_of_string s with
  | 0 -> `Empty
  | 1 -> `Shape
  | 2 -> `Block
  | _ -> `Solid
;;

let update_field bot v =
  bot.field <-
    Array.map (fun r ->
      Array.map (fun c ->
        cell_of_string c
      ) (Array.of_list (split_char ',' r))
    ) (Array.of_list (split_char ';' v))
;;

let position_of_string s =
  let tokens = split_char ',' s in
    (int_of_string (List.nth tokens 0), int_of_string (List.nth tokens 1))
;;

let piece_of_string = function
| "I" -> `I | "J" -> `J | "L" -> `L | "O" -> `O | "S" -> `S | "T" -> `T 
| "Z" ->`Z 
| v -> failwith ("Invalid piece: " ^ v ^ "\n")
;;

let four_token (gstate:game_state) key t1 t2 t3 =
   if (t3 = "") || (t2 = "") || (t1 = "") || (key = "")  then (
     debug ("four_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ t3 ^ "\n")
   ) else (
   match key with
    | "update" ->
      begin match t1 with
       | "game" ->
         begin match t2 with
          | "round" -> gstate.round <- int_of_string t3
          | "this_piece_type" -> gstate.this_piece_type <- piece_of_string t3
          | "next_piece_type" -> gstate.next_piece_type <- piece_of_string t3
          | "this_piece_position" -> gstate.this_piece_position <- position_of_string t3
          | _ -> ()
         end
       | bot -> begin match t2 with
          | "row_points" -> update_row_points (get_bot gstate bot) t3
          | "combo" -> update_combo (get_bot gstate bot) t3
          | "skips" -> update_skips (get_bot gstate bot) t3
          | "field" -> update_field (get_bot gstate bot) t3
          | _ -> debug ("Incorrect bot input: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ t3 ^ "\n")
         end
      end
    | _ -> ()
    )
;;

let my_name gstate = gstate.setup.your_bot;;

let set_player_names gstate v =
  gstate.setup.player_names <- split_char ',' v;
;;

let set_your_botname gstate v =
  gstate.setup.your_bot <- v;
(*
  let bot1 = get_bot gstate (List.nth gstaste.setup.player_names 0) in
  let bot2 = get_bot gstate (List.nth gstaste.setup.player_names 1) in
*)
;;

let three_token bot gstate key t1 t2 =
   if (t2 = "") || (t1 = "") || (key = "")  then ( 
     debug ("three_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ "\n")
   ) else (
     match key with
      | "settings" -> 
        begin match t1 with
         | "timebank" -> gstate.setup.timebank <- int_of_string t2
         | "time_per_move" -> gstate.setup.time_per_move <- int_of_string t2
         | "player_names" -> set_player_names gstate t2
         | "your_bot" -> set_your_botname gstate t2
	 | "field_width" -> gstate.setup.field_width <- int_of_string t2
	 | "field_height" -> gstate.setup.field_height <- int_of_string t2
         | _ -> ()
        end
      | "action" -> 
        begin match t1 with
         | "move" -> action_move bot gstate t2
         | _ -> ()
        end
      | _ -> ()
   )
;;

let process_line bot gstate line =
  debug (line ^ "\n");
  let tokens = split_char ' ' (uncomment line) in
  match List.length tokens with
  | 4 -> four_token gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2) (List.nth tokens 3)
  | 3 -> three_token bot gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2)
  | _ -> debug ("Incorrect bot input: " ^ line ^ "\n")
;;

let read_lines bot gstate =
  while true do
    let line = read_line () in
      process_line bot gstate line;
  done
;;

(* End input section *)

(* output section *)

let issue_order s =
   Printf.printf "%s\n" s;
   flush stdout;
;;

(* End output section *)

(* Utility functions *)

let random_from_list lst =
  let len = List.length lst in
    List.nth lst (Random.int len)
;;

let time_elapsed_this_turn gstate =
  (get_time() -. gstate.last_update) *. 1000.
;;

let time_remaining gstate =
  (float_of_int gstate.last_timebank -. time_elapsed_this_turn gstate)
;;

(* End utility *)

let new_player () =
{
  row_points = 0;
  combo = 0;
  skips = 0;
  field = [| [| |] |];
}
;;

let run_bot bot =
  (* Remove or change the self_init if you want predictable random sequences *)
  Random.self_init (); 

  let game_info =
     {
      timebank = 0;
      time_per_move = 0;
      player_names = [];
      your_bot = "";
      field_width = 0;
      field_height = 0;
     }
  in
  let game_state =
     {
      round = 0;
      this_piece_type = `I;
      next_piece_type = `I;
      this_piece_position = (0, 0);
      me = new_player();
      opponent = new_player();
      last_update = 0.0;
      last_timebank = 0;
      setup = game_info;
     }
  in

  begin try
   (
     read_lines bot game_state
   )
  with exc ->
   (
    debug (Printf.sprintf
       "Exception in turn %d :\n" game_state.round);
    debug (Printexc.to_string exc);
    raise exc
   )
  end;
;;


